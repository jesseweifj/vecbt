### Lightly vectorization back-test framework

1. Multiple order matching mechanisms
2. Multiple optimization targets
3. Back test strategy with fully vectorization to speed up
4. integrate with empyrical