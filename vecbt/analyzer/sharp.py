#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt.analyzer.analyzer import VecAnalyzer
from vecbt.core import NavSeries, AnalyzerAbbr
import pandas as pd


class SharpAnalyzer(VecAnalyzer):
    TAG = AnalyzerAbbr.SHARP

    def calculate_fitness(self, nav: pd.Series):
        s = NavSeries(nav)
        return s.sharpe_ratio()
