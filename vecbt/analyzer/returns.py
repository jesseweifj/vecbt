#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt.analyzer.analyzer import VecAnalyzer
from vecbt.core import NavSeries, AnalyzerAbbr
import pandas as pd


class ReturnAnalyzer(VecAnalyzer):
    TAG = AnalyzerAbbr.RETURN

    def calculate_fitness(self, nav: pd.Series):
        return nav[-1]
