#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt.analyzer.analyzer import VecAnalyzer
from vecbt.analyzer.returns import ReturnAnalyzer
from vecbt.analyzer.sharp import SharpAnalyzer
from vecbt.analyzer.comprehensive import ComprehensiveAnalyzer


class VecAnalyzerFactory(object):
    def __new__(cls, name) -> VecAnalyzer:
        _instance = VecAnalyzer.factory_create(name)
        return _instance
