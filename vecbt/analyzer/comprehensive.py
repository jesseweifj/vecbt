#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt.analyzer.analyzer import VecAnalyzer
from vecbt.core import NavSeries, AnalyzerAbbr
import pandas as pd
import logging


class ComprehensiveAnalyzer(VecAnalyzer):
    TAG = AnalyzerAbbr.COMP

    def calculate_fitness(self, nav: pd.Series):
        nav_series = NavSeries(nav)
        # nav_series.draw_growth()
        logging.debug(nav_series.describe())
        return nav_series.sharpe_ratio()
