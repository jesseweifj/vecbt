#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
from vecbt.core import Registered


class VecAnalyzer(Registered):

    def configure(self, analyzer_config: dict):
        for key, value in analyzer_config.items():
            setattr(self, key, value)

    def calculate_fitness(self, nav: pd.Series):
        pass
