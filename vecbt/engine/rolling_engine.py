#!/usr/bin/env python
# -*- coding: utf-8 -*-


from vecbt.engine.engine import BackTestEngineFactory
from vecbt.core import DataSet, BackEngineAbbr
from vecbt.engine.engine import BackTestEngine
import typing
import pandas as pd
import logging


class RollingEngine(BackTestEngine):

    TAG = BackEngineAbbr.ROLLING

    def optimize(self, params_list: typing.Iterable, data_set: DataSet, opt_config: dict):
        rolling_delta = pd.to_timedelta(opt_config['rolling_delta'])
        sample_delta = pd.to_timedelta(opt_config['sample_delta'])
        inner_engine_config: dict = opt_config['inner_engine_config']

        start_date = data_set.datetime[0]
        end_date = data_set.datetime[-1]

        simple_engine = BackTestEngineFactory(opt_config['inner_engine_type'])

        sample_start = start_date
        sample_end = start_date + sample_delta

        result_list = []
        arguments = None
        n_rolling_count = 0

        while sample_end <= end_date:
            sample_data_set = data_set[sample_start: sample_end]
            simple_engine.set_component(self.strategy, self._matcher, self._analyzer)
            fitness, arguments = simple_engine.optimize(params_list, sample_data_set, inner_engine_config)

            logging.debug('%s get period [%s, %s] fitness %f with parameter %s',
                          self,  sample_start, sample_end, fitness, arguments)
            result_list.append(fitness)

            sample_start = sample_start + rolling_delta
            sample_end = sample_start + sample_delta

            n_rolling_count += 1

        if len(result_list):
            average_fitness = sum(result_list) / len(result_list)
            logging.debug('%s get average fitness %f for %d round rolling test', self, average_fitness, n_rolling_count)
            return average_fitness, arguments
        else:
            return 0, {}

