#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt.engine.engine import BackTestEngine
from vecbt.core import DataSet, BackEngineAbbr
import typing
import copy
import logging


class SimpleEngine(BackTestEngine):
    TAG = BackEngineAbbr.SIMPLE

    def optimize(self, params_list: typing.Iterable, data_set: DataSet, opt_config: dict):
        mid_date = opt_config.get('mid_date')
        out_sample_percent = opt_config.get('out_sample_percent', 0.0)

        if out_sample_percent > 0 and mid_date is None:
            start_date = data_set.datetime[0]
            end_date = data_set.datetime[-1]

            time_delta = end_date - start_date
            mid_date = start_date + time_delta * (1.0 - out_sample_percent)

        if mid_date is None:
            in_sample_data_set = data_set
            out_sample_data_set = None
        else:
            in_sample_data_set = data_set[:mid_date]
            out_sample_data_set = data_set[mid_date:]

        best_fitness = float('-inf')
        best_arguments = None

        for params in params_list:
            self.strategy.configure(params)
            self.set_data_set(in_sample_data_set)
            self.strategy.fit()
            fitness = self.run()
            arguments = copy.copy(params)

            if best_arguments is None:
                best_arguments = params
            if fitness > best_fitness:
                best_fitness = fitness
                best_arguments = arguments

        logging.debug("%s get best fitness %12.4f for %s [%s, %s]",
                      self, best_fitness, self.strategy,
                      data_set.datetime[0], data_set.datetime[-1]
                      )

        if out_sample_data_set is not None:
            self.strategy.configure(best_arguments)
            self.set_data_set(out_sample_data_set)
            logging.debug("Out sample result:")
            best_fitness = self.run()
            logging.debug("")

        return best_fitness, best_arguments
