#!/usr/bin/env python
# -*- coding: utf-8 -*-
from vecbt.strategy import VecStrategyTemplate
from vecbt.analyzer import VecAnalyzer, VecAnalyzerFactory
from vecbt.matcher import VecMatcher, VecMatcherFactory
from vecbt.core import DataSet, Registered
import typing
import pandas as pd
import logging


class BackTestEngine(Registered):

    @property
    def strategy(self):
        return self._strategy

    def __repr__(self):
        return '%s()' % (self.__class__.__name__, )

    def __init__(self):
        self._strategy: VecStrategyTemplate = None
        self._analyzer: VecAnalyzer = None
        self._matcher: VecMatcher = None

        self._result_tuple: typing.Tuple[pd.DataFrame, pd.Series, float] = ()

    def set_data_set(self, data_set: DataSet):
        self._strategy.set_data_set(data_set)
        self._matcher.set_data_set(data_set)

    def set_component(self, strategy: VecStrategyTemplate, matcher: VecMatcher, analyzer: VecAnalyzer):
        self._strategy = strategy
        self._analyzer = analyzer
        self._matcher = matcher

    def result_tuple(self):
        return self._result_tuple

    def run(self):
        weight_df = self._strategy.calculate_weight()
        nav_series = self._matcher.calculate_nav(weight_df)
        fitness = self._analyzer.calculate_fitness(nav_series)

        self._result_tuple = weight_df, nav_series, fitness
        logging.debug('%s get fitness %12.4f', self.strategy, fitness)
        return fitness

    def fit(self, data_set: DataSet=None):
        if data_set:
            self.set_data_set(data_set)
        self._strategy.fit()

    def optimize(self, params_list: typing.Iterable, data_set: DataSet, opt_config: dict):
        pass


class BackTestEngineFactory(object):
    def __new__(cls, name) -> BackTestEngine:
        _instance = BackTestEngine.factory_create(name)
        return _instance

    @classmethod
    def create_matcher(cls, matcher_config):
        matcher = VecMatcherFactory(matcher_config['matcher_type'])
        matcher.configure(matcher_config['matcher_config'])
        return matcher

    @classmethod
    def create_analyzer(cls, analyzer_config):
        analyzer = VecAnalyzerFactory(analyzer_config['analyzer_type'])
        analyzer.configure(analyzer_config['analyzer_config'])
        return analyzer

