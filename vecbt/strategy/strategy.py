#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt.core import DataSet
import pandas as pd


class VecStrategyTemplate(object):

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__, self._config)

    def __init__(self):
        self._data_set: DataSet = None
        self._config = {}

    def set_data_set(self, data_set):
        self._data_set = data_set
        self.pre_process()

    @property
    def data_set(self):
        return self._data_set

    def configure(self, strategy_config: dict):
        self._config = strategy_config
        for key, value in strategy_config.items():
            setattr(self, key, value)

    def fit(self):
        pass

    def calculate_weight(self) -> pd.DataFrame:
        pass

    def pre_process(self):
        pass
