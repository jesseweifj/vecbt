#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt.strategy.strategy import VecStrategyTemplate


class Model(object):
    def fit(self):
        pass

    def predict(self, data):
        pass


class ModelStrategy(VecStrategyTemplate):
    def __init__(self):
        super(ModelStrategy, self).__init__()
        self._model: Model = None
