#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt.core import DataSet, Registered
import pandas as pd


class VecMatcher(Registered):
    def __init__(self):
        self._data_set: DataSet = None

    @property
    def data_set(self):
        return self._data_set

    def set_data_set(self, data_set):
        self._data_set = data_set
        self.pre_process()

    def configure(self, matcher_config: dict):
        for key, value in matcher_config.items():
            setattr(self, key, value)

    def calculate_nav(self, weight_df: pd.DataFrame):
        pass

    def pre_process(self):
        pass
