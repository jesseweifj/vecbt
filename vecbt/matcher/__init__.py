#!/usr/bin/env python
# -*- coding: utf-8 -*-
from vecbt.matcher.matcher import VecMatcher
from vecbt.matcher.bar_matcher import OpenCloseMatcher, CloseMatcher


class VecMatcherFactory(object):
    def __new__(cls, name) -> VecMatcher:
        _instance = VecMatcher.factory_create(name)
        return _instance
