#!/usr/bin/env python
# -*- coding: utf-8 -*-
from vecbt.matcher.matcher import VecMatcher
from vecbt.core import MatcherAbbr
import pandas as pd


class CloseMatcher(VecMatcher):
    TAG = MatcherAbbr.CLOSE_CLOSE

    def __init__(self):
        super(CloseMatcher, self).__init__()
        self.fee_rate = 0.0
        self._return_df: pd.DataFrame = None

    def calculate_nav(self, weight_df: pd.DataFrame):
        growth_series = 1 + (self._return_df * weight_df).sum(axis=1)
        fee_series = weight_df.diff().fillna(0).abs().sum(axis=1) * self.fee_rate
        nav_series = (growth_series - fee_series).cumprod()
        return nav_series

    def pre_process(self):
        close_df = self.data_set.select_field('close').fillna(method='ffill')
        return_df = close_df / close_df.shift(1) - 1
        return_df = return_df.fillna(0)
        self._return_df = return_df


class OpenCloseMatcher(CloseMatcher):
    TAG = MatcherAbbr.OPEN_CLOSE

    def pre_process(self):
        close_df = self.data_set.select_field('close')
        open_df = self.data_set.select_field('open')
        return_df = close_df / open_df - 1
        return_df = return_df.fillna(0)
        self._return_df = return_df
