#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd


def score_to_weight(score_df: pd.DataFrame, count=None, percent=0.2,  shortable=True):
    # the lower score is, the lower rank is
    rank_df: pd.DataFrame = score_df.rank(axis=1)
    valid_count = rank_df.count(axis=1)
    if count is None and percent is not None:
        upper_threshold = ((1 - percent) * valid_count).astype(int)
        lower_threshold = (percent * valid_count).astype(int)
    elif count is not None:
        upper_threshold = valid_count - count
        lower_threshold = count
    else:
        raise ValueError('please provide count or percent value')

    long_df = rank_df.gt(upper_threshold, axis=0)
    short_df = rank_df.lt(lower_threshold, axis=0)

    long_weight = long_df.divide(long_df.sum(axis=1), axis=0).fillna(0)

    if shortable:
        short_weight = -short_df.divide(short_df.sum(axis=1), axis=0).fillna(0)
        total_weight = long_weight + short_weight
    else:
        total_weight = long_weight

    # shift one period since trading begin at next period
    return total_weight.shift(1)