#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import xarray as xr


class DataArrayMaker(object):

    @staticmethod
    def from_df_dict(df_dict: dict, dim: str):
        data_array_dict = {}
        for key, df in df_dict.items():
            data_array_dict[key] = xr.DataArray(df)
        data_array = xr.Dataset(data_array_dict).to_array(dim=dim)
        return data_array

    @staticmethod
    def from_hdf(hdf_path: str, dim: str):
        store = pd.HDFStore(hdf_path)
        df_dict = {}
        for key in store.keys():
            df_dict[key] = store[key]
        store.close()
        return DataArrayMaker.from_df_dict(df_dict, dim)

