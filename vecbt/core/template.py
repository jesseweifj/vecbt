#!/usr/bin/env python
# -*- coding: utf-8 -*-
import warnings


class Registered(object):
    TAG = ''
    subclass_dict = {}

    def __init_subclass__(cls,**kwargs):
        super(Registered, cls).__init_subclass__(**kwargs)
        if Registered in cls.__bases__:
            cls.subclass_dict = {}
        else:
            if cls.TAG in cls.subclass_dict:
                msg = 'Tag "{}" already assigned to {}\n' \
                      'Please consider the risk of overriding'.format(cls.TAG, cls.subclass_dict[cls.TAG])
                warnings.warn(msg)
            cls.subclass_dict[cls.TAG] = cls

    @classmethod
    def factory_create(cls, tag):
        _class = cls.subclass_dict[tag]
        _instance = _class()
        return _instance
