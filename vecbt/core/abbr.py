#!/usr/bin/env python
# -*- coding: utf-8 -*-


class AnalyzerAbbr(object):
    SHARP = 'sharp'
    RETURN = 'return'
    COMP = 'comprehensive'


class MatcherAbbr(object):
    OPEN_CLOSE = 'open'
    CLOSE_CLOSE = 'close'


class BackEngineAbbr(object):
    ROLLING = 'rolling'
    SIMPLE = 'simple'
    MODEL = 'model'
