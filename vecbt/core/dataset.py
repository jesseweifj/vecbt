#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import xarray as xr
import typing

DATETIME = 'datetime'
SYMBOL = 'symbol'
FIELD = 'field'


class DataSet(object):

    def __init__(self):
        self._data_array: xr.DataArray = None

    @property
    def data(self):
        if self._data_array is None:
            raise RuntimeError('Please set data feed before using')
        return self._data_array

    def _from_dict(self, df_dict, dim):
        data_array_dict = {}

        if dim == SYMBOL:
            index_name = DATETIME
            column_name = FIELD
        elif dim == FIELD:
            index_name = DATETIME
            column_name = SYMBOL
        elif dim == DATETIME:
            index_name = SYMBOL
            column_name = FIELD
        else:
            raise TypeError('dim should be one of %s' % [DATETIME, FIELD, SYMBOL])

        for key, df in df_dict.items():
            df.index.name = index_name
            df.columns.name = column_name
            data_array_dict[key] = xr.DataArray(df)

        data_set = xr.Dataset(data_array_dict)
        data_array = data_set.to_array(dim)
        self.set_data_array(data_array)

    def set_data_array(self, data_array: xr.DataArray):
        data_array = data_array.transpose(DATETIME, SYMBOL, FIELD)
        self._data_array = data_array

    def from_symbol_dict(self, df_dict: typing.Dict[str, pd.DataFrame]):
        return self._from_dict(df_dict, SYMBOL)

    def from_field_dict(self, df_dict: typing.Dict[str, pd.DataFrame]):
        return self._from_dict(df_dict, FIELD)

    def from_time_dict(self, df_dict: typing.Dict[str, pd.DataFrame]):
        return self._from_dict(df_dict, DATETIME)

    def _select(self, select_dict: dict):
        return self._data_array.loc[select_dict]

    @property
    def datetime(self):
        return self._data_array.coords[DATETIME].values

    @property
    def symbol(self):
        return self._data_array.coords[SYMBOL].values

    @property
    def field(self):
        return self._data_array.coords[FIELD].values

    def select_datetime(self, dt)-> pd.DataFrame:
        return self._select({DATETIME: dt}).to_pandas()

    def select_symbol(self, symbol)-> pd.DataFrame:
        return self._select({SYMBOL: symbol}).to_pandas()

    def select_field(self, field_)-> pd.DataFrame:
        return self._select({FIELD: field_}).to_pandas()

    def __getitem__(self, item):
        data_array = self._data_array.loc[item]
        data_set = DataSet()
        data_set.set_data_array(data_array)
        return data_set

    def append(self, data_array: xr.DataArray, dim: str):
        data_array = data_array.transpose(DATETIME, SYMBOL, FIELD)
        full_data_array: xr.DataArray = xr.concat([self._data_array, data_array], dim=dim)
        self._data_array = full_data_array
