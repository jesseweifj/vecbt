import datetime
import pandas as pd
from collections import OrderedDict
import matplotlib.pyplot as plt
import empyrical
pd.core.common.is_list_like = pd.api.types.is_list_like


class NavSeries(object):
    """
        The main evaluation indicators are depends on module "empyrical" of package zipline.
            The main improvements of this class include
            1. Don't need to care about the frequency of data.
            2. Evaluation indicators can be annual indicators or semiannual ones if you call the class method "set_annual_unit"

        Note that you should provide one pd.Series object with datetime index to initialize this class.
        """
    Annual_Unit = datetime.timedelta(365)

    @classmethod
    def set_annual_unit(cls, timedelta):
        if not isinstance(timedelta, datetime.timedelta):
            raise TypeError("The parameter should be datetime.timedelta! ")
        cls.Annual_Unit = timedelta

    @classmethod
    def _simple_return(cls, time_series):
        return time_series.pct_change()

    @classmethod
    def _growth(cls, time_series, base=1.0):
        return time_series / time_series.iloc[0] * base

    def __init__(self, time_series):
        """
        Initialization
        :param time_series: pd.Series
        """
        if not isinstance(time_series, (pd.Series, pd.DataFrame)):
            raise TypeError("The time_series should be type pd.Series or pd.DataFrame!！")
        time_series = time_series.dropna().sort_index()
        if time_series.index.duplicated().any():
            raise TypeError("Time index duplicates!! ")
        if len(time_series) < 2:
            raise TypeError("Number of valid data is less than 2!! ")

        self._time_series = time_series
        self._frequency = self._annual_factor()
        self._returns = self._simple_return(self._time_series)

    def _annual_factor(self):
        """
        Returns annual factor which is equal to data frequency per year
        :return:  float
        """
        num_years = (self._time_series.index[-1] - self._time_series.index[0]) / self.Annual_Unit
        return len(self._time_series) / num_years

    def accumulate_return(self):
        return empyrical.cum_returns_final(self._returns)

    def aggregate_returns(self, convert_to):
        return empyrical.aggregate_returns(self._returns, convert_to)

    def max_drawback(self):
        return empyrical.max_drawdown(self._returns)

    def annual_return(self):
        return empyrical.annual_return(self._returns, annualization=self._frequency)

    def annual_volatility(self):
        return empyrical.annual_volatility(self._returns, annualization=self._frequency)

    def sortino_ratio(self):
        return empyrical.sortino_ratio(self._returns, annualization=self._frequency)

    def sharpe_ratio(self, risk_free=0.0):
        return empyrical.sharpe_ratio(self._returns, risk_free, annualization=self._frequency)

    def excess_sharpe(self, factor_series):
        factor_returns = self._simple_return(factor_series)
        return empyrical.excess_sharpe(self._returns, factor_returns)

    def value_at_risk(self, cutoff=0.05):
        return empyrical.value_at_risk(self._returns, cutoff)

    def conditional_value_at_risk(self, cutoff=0.05):
        return empyrical.conditional_value_at_risk(self._returns, cutoff)

    def beta(self, factor_series, risk_free=0.0):
        factor_returns = self._simple_return(factor_series)
        return empyrical.beta(self._returns, factor_returns, risk_free)

    def alpha(self, factor_series, risk_free=0.0, _beta=None):
        factor_returns = self._simple_return(factor_series)
        return empyrical.alpha(self._returns, factor_returns, annualization=self._frequency,
                               risk_free=risk_free, _beta=_beta)

    def draw_growth(self, aligned=False, factor_series=None):
        """
        Draws the old_strategy value growth path.
            If factors_series was provided, function would draw them both.
        :param factor_series:  pd.Series
        :param aligned: bool
        :return: None
        """
        fz = 12
        # fig,(ax1,ax2) =plt.subplots(figsize = (10,8), )
        fig = plt.figure()
        growth = self._time_series
        if aligned:
            growth = self._growth(self._time_series)
        ax = growth.plot(kind="line", linewidth=1, c='b')
        plt.title(r"$Strategy\ Growth$", fontsize=fz)
        plt.ylabel(r"$Growth$", fontsize=fz)
        ax.grid(False)

        if factor_series is not None:
            factor_growth = factor_series
            if aligned:
                factor_growth = self._growth(factor_series)
            plt.plot(factor_growth, c='r')
            f_name = factor_growth.name
            if f_name is None:
                f_name = r"Factor"
            plt.legend(["Strategy", f_name], loc="best")

        plt.xlabel(r"$Date$", fontsize=fz)
        # plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%Y%m%d'))
        plt.gcf().autofmt_xdate()
        plt.show()

        return fig

    def draw_simple_returns(self):
        """
        Draws the distribution of the simple returns
        :return: None
        """
        fz = 12
        fig = plt.figure()
        ret = self._returns.dropna()
        ret.plot(kind="kde", c="b", linewidth=2)
        n, bins, patches = plt.hist(ret, 20, normed=1, facecolor='g', alpha=0.75)
        plt.xlabel(r'$Return\ Rate$', fontsize=fz)
        plt.ylabel(r'$Probability$', fontsize=fz)
        plt.title(r'$Histogram\ of\ Return\ Rate$', fontsize=fz)
        plt.text(bins[0], max(n),
                 r'$\mu={:.4f}\%, \ \sigma={:.4f}\%$'.format(ret.mean() * 100, ret.std() * 100),
                 fontsize=fz)
        plt.grid(True)
        plt.show()
        return fig

    def draw_aggregate_returns(self, convert_to):
        convert_to = convert_to.lower()
        agg = self.aggregate_returns(convert_to)

        fig = plt.figure()
        ax = agg.plot(kind="bar")
        labels = agg.index.values
        x_ticks = ax.get_xticks()
        num = len(x_ticks)
        step = max(int(num / 12), 1)

        new_x_ticks = x_ticks[range(0, num, step)]
        new_x_labels = labels[range(0, num, step)]
        _ = ax.xaxis.set_ticks(new_x_ticks)
        _ = ax.set_xticklabels(new_x_labels)

        y_ticks = ax.get_yticks()
        new_y_ticks = map(lambda x: "{:.2%}".format(x), y_ticks)
        _ = ax.set_yticklabels(new_y_ticks)

        fig.autofmt_xdate()
        _ = ax.set_title(convert_to.title() + " Aggregate Returns")
        plt.show()
        return fig

    def describe(self):
        ind_dict = OrderedDict(
            [
                ('total return', self.accumulate_return()),
                ('annual return', self.annual_return()),
                ('annual vol', self.annual_volatility()),
                ('sharpe ratio', self.sharpe_ratio()),
                ('max drawback', self.max_drawback()),
            ]
        )

        header = "======: Performance profile: ======"
        values = [header]
        date_fmt = "{:>14s} {!s:>24}"
        values.append(date_fmt.format('start', self._time_series.index[0]))
        values.append(date_fmt.format('end', self._time_series.index[-1]))
        rate_fmt = "{:>14s} {:15.2%}"
        for key, value in ind_dict.items():
            values.append(rate_fmt.format(key, value))
        status = '\n'.join(values)
        return status
