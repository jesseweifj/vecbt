#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt import VecStrategyTemplate
import numpy as np
import pandas as pd
from sklearn import linear_model


class SVMStrategy(VecStrategyTemplate):

    def __init__(self):
        super(SVMStrategy, self).__init__()
        self._model = None

    def _get_x_y(self):
        full_data_array = self.data_set.data
        close_1m = self.data_set.select_field('close')
        future_return = np.log(close_1m).diff().shift(-1)
        y = future_return.stack(dropna=False)
        X = full_data_array.to_dataframe('data').unstack()

        y_index = y.notnull()
        X_index = X.notnull().all(axis=1)

        X_index.sum()
        y_index.sum()

        fil_index = X_index & y_index

        X_1 = X[fil_index]
        y_1 = y[fil_index]

        return X_1, y_1

    def fit(self):
        X_1, y_1 = self._get_x_y()
        model = linear_model.LinearRegression()
        model.fit(X_1, y_1)
        self._model = model

    def calculate_weight(self):
        X_1, y_1 = self._get_x_y()
        y_2 = y_1.copy()
        y_2[:] = self._model.predict(X_1)

        score_df: pd.DataFrame = y_2.unstack()
        down_threshold = score_df.mean(axis=1)
        upper_threshold = score_df.mean(axis=1)

        long_df = score_df.ge(upper_threshold, axis=0)
        short_df = score_df.lt(down_threshold, axis=0)

        long_weight = long_df.divide(long_df.sum(axis=1), axis=0).fillna(0)
        short_weight = -short_df.divide(short_df.sum(axis=1), axis=0).fillna(0)

        total_weight = long_weight + short_weight

        # shift to set future weight
        total_weight = total_weight.shift(1)
        # total_weight = total_weight.\
        #     reindex(index=close_1m.index, fill_value=0.0).\
        #     reindex(columns=close_1m.columns, fill_value=0.0)

        return total_weight
