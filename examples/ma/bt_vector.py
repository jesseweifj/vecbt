#!/usr/bin/env python
# -*- coding: utf-8 -*-
from vecbt.core.dataset import DataSet
from vecbt.engine import BackTestEngineFactory
from examples.ma.vec_ma import VecMAStrategy

import json
import pandas as pd
import logging

import os
os.chdir('ma')
logging.getLogger().setLevel('DEBUG')


if __name__ == '__main__':
    config_path = r'./bt_vector.json'
    # config_path = r'./bt_vector_2.json'
    with open(config_path) as f:
        bt_config = json.load(f)

    bar_df = pd.read_csv('bar.csv')
    bar_df['datetime'] = pd.to_datetime(bar_df['datetime'])
    bar_df.set_index('datetime', inplace=True)
    df_dict = {
        'ETHUSD': bar_df
    }

    data_set = DataSet()
    data_set.from_symbol_dict(df_dict)

    engine_config = bt_config['engine']
    bt_engine = BackTestEngineFactory(engine_config['engine_type'])
    opt_config = engine_config['opt_config']

    analyzer = BackTestEngineFactory.create_analyzer(bt_config['analyzer'])
    matcher = BackTestEngineFactory.create_matcher(bt_config['matcher'])
    strategy = VecMAStrategy()
    bt_engine.set_component(strategy, matcher, analyzer)

    strategy.configure({})
    bt_engine.set_data_set(data_set)
    bt_engine.fit()
    bt_engine.run()


    def params_generator():
        for sma in range(5, 10):
            for lma in range(15, 20):
                yield {
                    'n_sma': sma,
                    'n_lma': lma,
                }
    #
    bt_engine.optimize(list(params_generator()), data_set, opt_config)


"""ilovek
Buy  ETHUSD at 2018-08-31 23:00:00
Sell ETHUSD at 2018-09-02 10:00:00
Buy  ETHUSD at 2018-09-03 04:00:00
Sell ETHUSD at 2018-09-03 08:00:00
Buy  ETHUSD at 2018-09-04 00:00:00
Sell ETHUSD at 2018-09-04 08:00:00
Buy  ETHUSD at 2018-09-04 22:00:00
Sell ETHUSD at 2018-09-05 02:00:00
Buy  ETHUSD at 2018-09-07 01:00:00
Sell ETHUSD at 2018-09-07 04:00:00
Buy  ETHUSD at 2018-09-07 05:00:00
Sell ETHUSD at 2018-09-07 14:00:00
Buy  ETHUSD at 2018-09-08 10:00:00
Sell ETHUSD at 2018-09-08 16:00:00
Buy  ETHUSD at 2018-09-09 15:00:00
Sell ETHUSD at 2018-09-10 04:00:00
Buy  ETHUSD at 2018-09-11 06:00:00
Sell ETHUSD at 2018-09-11 13:00:00
Buy  ETHUSD at 2018-09-12 22:00:00
Sell ETHUSD at 2018-09-14 16:00:00
Buy  ETHUSD at 2018-09-15 00:00:00
======: Performance profile: ======
         start      2018-08-31 00:00:00
           end      2018-09-15 00:00:00
  total return           9.34%
 annual return         777.62%
    annual vol          64.79%
  sharpe ratio         368.40%
  max drawback         -11.17%
"""