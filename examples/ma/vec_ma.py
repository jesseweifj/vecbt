#!/usr/bin/env python
# -*- coding: utf-8 -*-

from vecbt import VecStrategyTemplate


class VecMAStrategy(VecStrategyTemplate):

    def __init__(self):
        super(VecMAStrategy, self).__init__()
        self.n_sma = 13
        self.n_lma = 20

    def calculate_weight(self):
        close_df = self.data_set.select_field('close')

        # calculate signals
        sma_df = close_df.rolling(self.n_sma).mean()
        lma_df = close_df.rolling(self.n_lma).mean()

        pre_sma_df = sma_df.shift(1)
        pre_lma_df = lma_df.shift(1)

        # trade base on the information of yesterday
        should_long = ((sma_df > lma_df) & (pre_sma_df < pre_lma_df)).shift()
        should_long.iloc[0, :] = False
        should_short = ((sma_df < lma_df) & (pre_sma_df > pre_lma_df)).shift()
        should_short.iloc[0, :] = False

        # generate weight
        weight_df = should_long.cumsum() - should_short.cumsum()
        return weight_df
