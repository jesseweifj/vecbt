#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Back test framework with full vectorization speed up
"""

from setuptools import setup, find_packages
import vecbt


def extract_packages(package):
    packages = find_packages(package)
    packages = list(map(lambda x: package + '.' + x, packages))
    packages.append(package)
    return packages


setup(
    name='vecbt',
    version=vecbt.__version__,
    author=vecbt.__author__,
    description='Lightly and fully-vectorization Back test framework',
    long_description=__doc__,
    keywords=['back test', 'development'],
    author_email='jesseweifj@gmail.com',
    packages=extract_packages('vecbt'),
    include_package_data=True,
    install_requires=[
        'ipython',
        'numpy',
        'pandas',
        'matplotlib',
        'xarray',
        'empyrical'
    ],
)
